import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_postgres_is_installed(host):
    print("""
    ***
    This module test: 
    - postgresql package is installed
    - postgres service is running
    - service is enabled
    ***
    """)
    postgres = host.package("postgresql")
    assert postgres.is_installed

def test_postgres_running_and_enabled(host):
    postgres = host.service("postgresql")
    assert postgres.is_running